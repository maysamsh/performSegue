//
//  ViewController.swift
//  PerformSegue
//
//  Created by Maysam Shahsavari on 5/3/16.
//  Copyright © 2016 Maysam Shahsavari. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func goto(sender: UIButton) {
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func unwindToVC(segue:UIStoryboardSegue) {
        
    }
            
    @IBAction func gotoNextVC(sender: UIButton) {
        performSegueWithIdentifier("GoToNextVC", sender: self)
    }

}

