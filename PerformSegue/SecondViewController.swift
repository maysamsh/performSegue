//
//  SecondViewController.swift
//  PerformSegue
//
//  Created by Maysam Shahsavari on 5/3/16.
//  Copyright © 2016 Maysam Shahsavari. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITextViewDelegate {

    @IBOutlet weak var textBox: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
            textBox.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(animated: Bool) {
        textBox.becomeFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
            self.textBox.selectedTextRange = self.textBox.textRangeFromPosition(self.textBox.beginningOfDocument, toPosition: self.textBox.endOfDocument)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
